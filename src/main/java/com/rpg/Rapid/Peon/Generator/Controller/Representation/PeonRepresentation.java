package com.rpg.Rapid.Peon.Generator.Controller.Representation;

import com.rpg.Rapid.Peon.Generator.Domain.Race;
import com.rpg.Rapid.Peon.Generator.Domain.Sex;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.UUID;

public class PeonRepresentation {

    private String id;
    private String name;
    private Sex sex;
    private int age;
    private Race race;
    private int strength;
    private int endurance;
    private int dexterity;
    private int intelligence;
    private int luck;

    public PeonRepresentation(String id,String name, Sex sex, int age, Race race, int strength, int endurance, int dexterity, int intelligence, int luck) {
        this.id=id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.race = race;
        this.strength = strength;
        this.dexterity = dexterity;
        this.endurance = endurance;
        this.intelligence = intelligence;
        this.luck = luck;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }

}
