package com.rpg.Rapid.Peon.Generator.Controller;

import com.rpg.Rapid.Peon.Generator.Controller.Representation.PeonRepresentation;
import com.rpg.Rapid.Peon.Generator.Controller.Representation.PlayerRepresentation;
import com.rpg.Rapid.Peon.Generator.Domain.Peon;
import com.rpg.Rapid.Peon.Generator.Domain.Player;
import com.rpg.Rapid.Peon.Generator.Service.PeonService;
import com.rpg.Rapid.Peon.Generator.Service.PlayerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping("/players")
public class PlayerController {

        PlayerService service;

        public PlayerController(PlayerService service) {
            this.service = service;
        }

        @PostMapping("/create")
        public String addPlayer(@RequestBody PlayerRepresentation body) {
            service.addPlayer(body);
            return "Votre compte a bien été enregistré";
        }

        @DeleteMapping("/delete/{id}")
        public String deletePlayer(@PathVariable(value = "id") String id) {
            service.deletePlayer(id);
            return "Le personnage a bien été supprimé";
        }

        @GetMapping
        public List<PlayerRepresentation> getAllPlayer() {
            List<PlayerRepresentation> players = new ArrayList<>();
            service.getAllPlayer();
            for (Player p : service.getAllPlayer()) {
                PlayerRepresentation player = new PlayerRepresentation(p.getId(),p.getPseudo(),p.getTag(),p.getPeons());
                players.add(player);
            }
            return players;
        }

        @GetMapping("/{id}")
        ResponseEntity<PlayerRepresentation> getOnePlayer(@PathVariable(value = "id") String id) {
            Optional<PlayerRepresentation> player = this.service.getOnePlayerRepresentation(id);
            return player.map(q->ResponseEntity.ok(q)).orElse(ResponseEntity.notFound().build());
        }
}
