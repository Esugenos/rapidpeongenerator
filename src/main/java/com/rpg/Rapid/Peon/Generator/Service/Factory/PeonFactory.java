package com.rpg.Rapid.Peon.Generator.Service.Factory;

import com.rpg.Rapid.Peon.Generator.Domain.Peon;
import com.rpg.Rapid.Peon.Generator.Domain.Race;
import com.rpg.Rapid.Peon.Generator.Domain.Sex;

import java.util.UUID;

public class PeonFactory {

    public PeonFactory(){

    }
    public Peon generatePeon(String name, Sex sex, int age, Race race, int strength, int endurance, int dexterity, int intelligence, int luck){
        String id = UUID.randomUUID().toString();
        return new Peon(id,name,sex,age,race,strength,endurance,dexterity,intelligence,luck);
    }

}
