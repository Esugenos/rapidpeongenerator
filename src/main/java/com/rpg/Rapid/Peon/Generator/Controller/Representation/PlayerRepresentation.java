package com.rpg.Rapid.Peon.Generator.Controller.Representation;

import com.rpg.Rapid.Peon.Generator.Domain.Peon;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

public class PlayerRepresentation {

    String id;
    String pseudo;
    String tag;
    List<Peon> peons;

    public PlayerRepresentation(String id, String pseudo, String tag, List<Peon> peons) {
        this.id = id;
        this.pseudo = pseudo;
        this.tag = tag;
        this.peons = peons;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<Peon> getPeons() {
        return peons;
    }

    public void setPeons(List<Peon> peons) {
        this.peons = peons;
    }
}
