package com.rpg.Rapid.Peon.Generator.Domain;

public enum Race {
    ELF, ORC, HUMAN, DWARF;
}
