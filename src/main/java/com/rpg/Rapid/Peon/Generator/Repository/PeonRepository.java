package com.rpg.Rapid.Peon.Generator.Repository;

import com.rpg.Rapid.Peon.Generator.Domain.Peon;
import org.springframework.data.repository.CrudRepository;

public interface PeonRepository extends CrudRepository<Peon, String> {
}
