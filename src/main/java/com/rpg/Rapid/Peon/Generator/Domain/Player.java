package com.rpg.Rapid.Peon.Generator.Domain;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Entity
public class Player {

    @Id
    String id;
    String pseudo;
    String tag;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_player")
    List<Peon> peons;

    public Player() {
    }

    public Player(String id, String pseudo, String tag, List<Peon> peons) {
        this.id = id;
        this.pseudo = pseudo;
        this.tag = tag;
        this.peons = peons;

    }

    public void addPeonToPlayer(Peon peon) {
        peons.add(peon);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<Peon> getPeons() {
        return peons;
    }

    public void setPeons(List<Peon> peons) {
        this.peons = peons;
    }

}
