package com.rpg.Rapid.Peon.Generator.Controller;

import com.rpg.Rapid.Peon.Generator.Controller.Representation.PeonRepresentation;
import com.rpg.Rapid.Peon.Generator.Domain.Peon;
import com.rpg.Rapid.Peon.Generator.Service.PeonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/peons")
public class PeonController {

    PeonService service;

    public PeonController(PeonService service) {
        this.service = service;
    }

    @PostMapping("/create")
    public String addPeon(@RequestBody PeonRepresentation body) {
        service.addPeon(body);
        return "Votre personnage a bien été enregistré";
    }

    @DeleteMapping("/delete/{id}")
    public String deletePeon(@PathVariable(value = "id") String id) {
        service.deletePeon(id);
        return "Le personnage a bien été supprimé";
    }

    @GetMapping
    public List<PeonRepresentation> getAllPeons() {
        List<PeonRepresentation> peons = new ArrayList<>();
        service.getAllPeon();
        for (Peon p : service.getAllPeon()) {
            PeonRepresentation peon = new PeonRepresentation(p.getId(), p.getName(), p.getSex(), p.getAge(), p.getRace(), p.getStrength(),
                    p.getEndurance(), p.getDexterity(), p.getIntelligence(), p.getLuck());
            peons.add(peon);
        }
        return peons;
    }

    @GetMapping("/{id}")
    ResponseEntity<PeonRepresentation> getOnePeon(@PathVariable(value = "id") String id) {
        Optional<PeonRepresentation> peon = this.service.getOnePeonRepresentation(id);
        return peon.map(q->ResponseEntity.ok(q)).orElse(ResponseEntity.notFound().build());
    }
    @PutMapping("/{idpe}/join/{idpl}")
    public String addPeonToPlayer(@PathVariable(value="idpe") String idpe,@PathVariable(value="idpl") String idpl){
        service.AddPeonToPlayer(idpe, idpl);
        return "Personnage ajouté à votre compte";
    }
}