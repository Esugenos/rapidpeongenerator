package com.rpg.Rapid.Peon.Generator.Service;

import com.rpg.Rapid.Peon.Generator.Controller.Representation.PeonRepresentation;
import com.rpg.Rapid.Peon.Generator.Controller.Representation.PlayerRepresentation;
import com.rpg.Rapid.Peon.Generator.Domain.Peon;
import com.rpg.Rapid.Peon.Generator.Domain.Player;
import com.rpg.Rapid.Peon.Generator.Repository.PlayerRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class PlayerService {
    PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void addPlayer(PlayerRepresentation player) {
        Player newPlayer = new Player(UUID.randomUUID().toString(), player.getPseudo(), "#"+UUID.randomUUID().toString(), null);
        playerRepository.save(newPlayer);
    }

    public void deletePlayer(String idPlayer) {
        playerRepository.deleteById(idPlayer);
    }
    public Optional<PlayerRepresentation>getOnePlayerRepresentation(String idPlayerR){
        Optional<Player> player =getOnePlayer(idPlayerR);
        return player.map(p -> new PlayerRepresentation(p.getId(),p.getPseudo(),p.getTag(),p.getPeons()));
    }


    private Optional<Player> getOnePlayer(String idPeon) {
        Optional<Player> player = playerRepository.findById(idPeon);
        return player;
    }
    public List<Player> getAllPlayer() {
        List<Player> players = new ArrayList<>();
        for (Player p : playerRepository.findAll()) {
            players.add(p);
        }
        return players;
    }
}
