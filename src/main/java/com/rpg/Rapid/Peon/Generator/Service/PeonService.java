package com.rpg.Rapid.Peon.Generator.Service;

import com.rpg.Rapid.Peon.Generator.Controller.Representation.PeonRepresentation;
import com.rpg.Rapid.Peon.Generator.Domain.Peon;
import com.rpg.Rapid.Peon.Generator.Domain.Player;
import com.rpg.Rapid.Peon.Generator.Repository.PeonRepository;
import com.rpg.Rapid.Peon.Generator.Repository.PlayerRepository;
import com.rpg.Rapid.Peon.Generator.Service.Factory.PeonFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class PeonService {

    PeonRepository peonRepository;
    PlayerRepository playerRepository;
    PeonFactory peonFactory;

    public PeonService(PeonRepository peonRepository, PlayerRepository playerRepository, PeonFactory peonFactory) {
        this.peonRepository = peonRepository;
        this.playerRepository = playerRepository;
        this.peonFactory=peonFactory;

    }

    public void addPeon(PeonRepresentation peon) {
        Peon newPeon = peonFactory.generatePeon(peon.getName(), peon.getSex(), peon.getAge(), peon.getRace(),
                peon.getStrength(), peon.getEndurance(), peon.getDexterity(), peon.getIntelligence(), peon.getLuck());
        peonRepository.save(newPeon);
    }

    public void deletePeon(String idPeon) {
        peonRepository.deleteById(idPeon);
    }
    public Optional<PeonRepresentation>getOnePeonRepresentation(String idPeonR){
        Optional<Peon> peon =getOnePeon(idPeonR);
               return peon.map(p -> new PeonRepresentation(p.getId(),p.getName(),p.getSex(),p.getAge(),p.getRace(),p.getStrength(),
                p.getEndurance(),p.getDexterity(),p.getIntelligence(),p.getLuck()));
    }


    private Optional<Peon> getOnePeon(String idPeon) {
        Optional<Peon> peon = peonRepository.findById(idPeon);
        return peon;
    }

    public List<Peon> getAllPeon() {
        List<Peon> peons = new ArrayList<>();
        for (Peon pe : peonRepository.findAll()) {
            peons.add(pe);
        }
        return peons;
    }

    public void AddPeonToPlayer(String idPeon, String idPlayer) {

        playerRepository.findById(idPlayer).ifPresent(p -> {
            peonRepository.findById(idPeon).ifPresent(pe -> p.addPeonToPlayer(pe));
            playerRepository.save(p);
        });
    }

    public String getTag() {
        String tag = "#" + UUID.randomUUID().toString();
        return tag;
    }
}
