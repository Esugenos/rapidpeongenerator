package com.rpg.Rapid.Peon.Generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RapidPeonGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(RapidPeonGeneratorApplication.class, args);
	}

}
