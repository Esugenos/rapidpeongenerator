package com.rpg.Rapid.Peon.Generator.Repository;

import com.rpg.Rapid.Peon.Generator.Domain.Player;
import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<Player, String> {
}
