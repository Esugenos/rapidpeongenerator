package com.rpg.Rapid.Peon.Generator.Domain;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Peon {

    @Id
    private String id;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private Sex sex;
    private int age;
    @Enumerated(value = EnumType.STRING)
    private Race race;
    private int strength;
    private int endurance;
    private int dexterity;
    private int intelligence;
    private int luck;

    public Peon() {
    }

    public Peon(String id,String name, Sex sex, int age, Race race, int strength, int endurance, int dexterity, int intelligence, int luck) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.race = race;
        this.strength = strength;
        this.dexterity = dexterity;
        this.endurance = endurance;
        this.intelligence = intelligence;
        this.luck = luck;

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Sex getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public Race getRace() {
        return race;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }

}
